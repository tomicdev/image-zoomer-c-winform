﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoAutoScroll
{
    public partial class Form1 : Form
    {

        private Image img = Image.FromFile(@"C:\Users\Petrol\Desktop\Nova mapa\GFS\InitModel-06Z_ValidModel-06Z-168.gif");

        public Form1()
        {
            InitializeComponent();

            panel1.VerticalScroll.Enabled = false;
            //panel1.VerticalScroll.Visible = false; - for some reason not working
            panel1.HorizontalScroll.Enabled = false;
            //panel1.HorizontalScroll.Visible = false;


            panel1.AutoScroll = true;

            pictureBox1.Dock = DockStyle.None; // must be None! otherwise it wont work
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox1.ClientSize = panel1.ClientSize;


            pictureBox1.Image = new Bitmap(img);
            pictureBox1.Cursor = Cursors.Hand;
            panel1.Controls.Add(pictureBox1);

            
            // For drag scrolling functionality
            pictureBox1.MouseMove += pictureBox1_MouseMove;
            pictureBox1.MouseDown += pictureBox1_MouseDown;
            pictureBox1.MouseUp += pictureBox1_MouseUp;

            pictureBox1.MouseWheel += pictureBox1_MouseWheel;
            pictureBox1.MouseHover += pictureBox1_MouseHover;
           

        }

        void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            pictureBox1.Focus();
        }

        private double imgZoomRatio = 1;

        void pictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            // prevent pictureBox to scroll
            ((HandledMouseEventArgs)e).Handled = true;

            // zoom in
            if (e.Delta > 0)
            {
                if(imgZoomRatio < 8)
                    imgZoomRatio += 0.2;
            }
            // zoom out
            else if (e.Delta < 0)
            {
                if(imgZoomRatio > 0.4) 
                    imgZoomRatio -= 0.2;
            }

            Console.WriteLine(imgZoomRatio);
            ImageZoom();
        }

        void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBox1.MouseDown -= pictureBox1_MouseDown;
            _lastX = e.X;
            _lastY = e.Y;
        }

        void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox1.MouseDown += pictureBox1_MouseDown;
            _lastX = -1;
            _lastY = -1;
        }

        static int _lastX = -1;
        static int _lastY = -1;

        private static bool dragging()
        {
            if (_lastX == -1 || _lastY == -1)
                return false;
            return true;
        }


        void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging())
            {
                if (e.Button == MouseButtons.Left)
                {
                    int realX = e.X + panel1.AutoScrollPosition.X;
                    int realY = e.Y + panel1.AutoScrollPosition.Y;

                    int deltaX = (realX - _lastX);
                    int deltaY = (realY - _lastY);

                    pictureBox1.Left += deltaX;
                    pictureBox1.Top += deltaY;

                } 
             
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Bitmap bitmap = new Bitmap(img);
            pictureBox1.Image = bitmap;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(img, (int)(img.Width * 1.5), (int)(img.Height * 1.5));    
            pictureBox1.Image = bitmap;
        }

        private void button3_Click(object sender, EventArgs e)
        {
          
        }

        private void ImageZoom()
        {
            Bitmap bitmap = new Bitmap(img, (int)(img.Width * imgZoomRatio), (int)(img.Height * imgZoomRatio));
            pictureBox1.Image = bitmap;
        }

       

    }
}
